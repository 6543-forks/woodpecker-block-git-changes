FROM alpine/git:2.36.2

ENTRYPOINT ["git", "diff", "--exit-code"]
