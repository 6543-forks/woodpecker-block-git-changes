---
name: Block Git changes
authors: qwerty287
description: Plugin to block uncommited  changes in the Git repository
tags: [git, changes]
containerImage: qwerty287/woodpecker-block-git-changes
containerImageUrl: https://hub.docker.com/r/qwerty287/woodpecker-block-git-changes
url: https://codeberg.org/qwerty287/woodpecker-block-git-changes
---

Plugin that fails if the Git repository contains any uncommited changes.

## Usage

```yaml
pipeline:
	block-changes:
		image: qwerty287/woodpecker-block-git-changes
```

This will never fail because there isn't any change done before.

```yaml
pipeline:
	echo:
		image: alpine
		commands:
			- echo "hello world" > README.md

    block-changes:
      image: qwerty287/woodpecker-block-git-changes
```

This will fail if your README.md doesn't already contain `hello world`.
