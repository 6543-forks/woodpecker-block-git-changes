# block-git-changes

Woodpecker plugin that blocks changes in the Git repository. Extremely simple, just fails if there are any changes in the working tree.
